//NALUWUJJA ASHLEY HOPE
//23/U/14174
//2300714174

#include <stdio.h>
#include <math.h>

// Function to calculate the discriminant
float calculateDiscriminant(float a, float b, float c) {
    return b * b - 4 * a * c;
}

// Function to determine the nature of roots
void determineRoots(float a, float b, float c) {
    float discriminant = calculateDiscriminant(a, b, c);

    // Check the nature of roots
    if (discriminant > 0) {
        // Real and distinct roots
        float root1 = (-b + sqrt(discriminant)) / (2 * a);
        float root2 = (-b - sqrt(discriminant)) / (2 * a);
        printf("Roots are real and distinct:\n");
        printf("Root 1 = %.2f\n", root1);
        printf("Root 2 = %.2f\n", root2);
    } else if (discriminant == 0) {
        // Real and equal roots
        float root = -b / (2 * a);
        printf("Roots are real and equal:\n");
        printf("Root 1 = Root 2 = %.2f\n", root);
    } else {
        // Complex roots
        printf("Complex roots.\n");
    }
}

int main() {
    float a, b, c;

    // Input coefficients from the user and validate a is not zero
    do {
        printf("Enter coefficient a (must be non-zero): ");
        scanf("%f", &a);

        if (a == 0) {
            printf("Coefficient 'a' must be non-zero. Please try again.\n");
        }
    } while (a == 0);

    printf("Enter coefficients b and c: ");
    scanf("%f %f", &b, &c);

    // Call the function to determine roots
    determineRoots(a, b, c);

    return 0;
}
